[Foja 1]
//1 
/1 Selo[?] reales.

/2 [Sello] SELLO SEGUNDO, SEIS REA-
/3 LES AÑOS DE MIL SETE-
/4 CIENTOS OCHENTA Y SEIS Y
/5 OCHENTA Y SIETE.

/6 [Sello] El Rey= Virrey, Governador, y Capitan Gene-
/7 ral de las Provincias del Río de la Plata, y Pre-
/8 -sidente de mi Real Audiencia de la Ciudad
/9 de Buenos Ayres. Por parte de Don Juan An-
/10 -tonio de Olmedo y don Domingo Bauzá
/11 dado Memorial, acompañando icho documen-
/12 -tos, y expresando que siendo en el año de mil
/13 setecientos ochenta y dos, Alcaldes ordinarios
/14 de la Ciudad de Montevideo les pasé el Gover-
/15 -nador Don Joaquín del Pino en once de febrero
/16 un oficio en que les pribaba de poner en 
/17 ejecucion las sentencias que como tales Al-
/18 -caldes, diesen en las Causas Criminales, pre-
/19 -viniendoles no lo hiciesen, sín darle antes
/20 cuenta de ellas, con Autos y que haviendo
/21 obserbado sé tomaba conocimiento por los
/22 Juzgados de los homicidíos, robos, heridas, 
/23 y otros casos, sín que por el Juez de las
/24 Causa se lé diese parte de lo acaesido,
/25 (lo que era opuesto de la Superioridad de su
/26 Govierno y producia infinitos abusos) 
/27 les mandaba que Inmediatamente

[Foja 2]
//2
/1 le comunicaran noticia, con la expresion
/2 correspondiente para una perfecta inteligen-
/3 -cia, y que para que los succesores en estos
/4 empleos no alegaran ignorancía se pusiere
/5 dicho Oficio en el Libro de Acuerdos, noticiandole
/6 se había practicado asi: Que en el dia veinte
/7 lé respondieron era opuesto quanto ordenaba
/8 al uso y antigua costumbre que hasta
/9 aquel tiempo sé habia obserbado, y que
/10 asimismo perjudiciaba á mi Real Audien-
/11 -cia de aquel distrito, á quien correspondia
/12 la prerrogatiba de comentarla en los
/13 referidos casos, lo que ponian en su noticia
/14 para que sobrecayese en su providencia
/15 sin hacer novedad hasta la resolucíon de
/16 la Audiencia á quien consultaban que
/17 no obstante esta respuesta les pasó segundo
/18 Oficío en el dia veinte y quatro reproducien-
/19 -do el primero y mandandoles dijeren sí obe-
/20 -decían, ó no. Que á este contestaron en
/21 veinte y siete expresando que mediante
/22 dudar sobre su respuesta, y no haber Profe-
/23 -sores de Derecho en aquella Plaza, determí-
/24 naban pasár su Oficío á dos Abogados [¿de, en?]
/25 Buenos-Ayres para poder contestar, sín
/26 perjudicár la Jurisdiccíon que como Alcal-
/27 des les correspondia: Qué lejos de conformarse
/28 con esta respuesta dio cuenta al Virrey

[Foja 3]
//3

/1 Para Juan José de Vertiz quien dío orden para
/2 que al Alcalde de primer voto le embiase [ilegible]
/3 -tado á la Ysla de [¿Gorrite?]] y al de segundo á la
/4 de aquel Puerto, en cuíos parages pedian espe-
/5 -rár el dictamen de los abogados, pero que antes
/6 de esto les llamó el Virrey, y fue solo para
/7 decirles palabras afrentosas, de lo que resultó
/8 que Don Juan Antonio Haedo perdió el Juicío
/9 por algunos dias, y que aun sín estár entera-
/10 -mente recuperado de este accidente ínsistío el
/11 Virrey en el destierro. Por todo lo qual hán replica-
/12 -do mé digne desaprobár lo obrado por el Governa-
/13 -dor y el Virrey, previniendoles que en lo futuro
/14 sé abstengan de semejantes procedimientos,
/15 y desde luego reintegren á los exponentes [¿de?]
/16 los daños y perjuicíos que se les han seguido
/17 en sus intereses, y quando á esto no haya
/18 lugar, se mande pasár esta histancía á Jus-
/19 -ticia; Y haviendose visto en mí Consejo ple-
/20 -no de Yndias, con lo expuesto por mi Fiscal, hé
/21 venido en declarár que los citados Oficíos del
/22 Governador de Montevideo, de once y veinte
/23 y quatro de febrero, en el modo y forma
/24 que están concebidos, son contraríos á la
/25 disposicíon de las Leyes por comprender abso-

[Foja 4]
//4

/1 -luta y generalmente toda especie de Causas
/2 criminales sín distíncion, pues unicamente
/3 deben dar noticía al Governador de las [¿de?]
/4 asonada ó comocíon popular que puedan
/5 turbár el sosiego de la Provincía, pero no
/6 de las [¿demas?] Criminales de que sé conosca
/7 en aquellos Juzgados pribatibamente, con-
/8 -forme á la disposicion de las Leyes, pues
/9 de estas solo deben darle cuenta sin Autos,
/10 y tambien se la deben dar de las Senten-
/11 cias que trataron executár con pena corpo-
/12 -ral; y por habér contravenido á la disposicion
/13 de las mismas Leyes expresamente, Impongo
/14 á dicho Governador docientos pesos de multa
/15 aplicados á penas [¿de Camarras?] y gastos [¿de?]
/16 Justicia del referido mi Consejo, la qual lé
/17 exigireis, como os lo mando, sin admitirle
/18 excepcion ni escusa alguna, y la tendreis á
/19 disposicíon del Juez de multas del referido mi
/20 Consejo, á quien por Despacho de esta fecha
/21 sé previene lo conveniente. Asimismo hé
/22 declarado que fueron mui conformes y
/23 arregladas las contestacíones de los Alcaldes
/24 Ordinaríos, á los dos citados Oficios del Gover-
/25 -nador, con las que sé debío aquietár, espe-

[Foja 5]
//5

/1 -rando las resultas de mi Real Audiencia,
/2 y no pasár á sorprender la autoridad [¿de?]
/3 vuestro Antecesor. Finalmente hé venido
/4 en reserbár á dichos dos Alcaldes Don Juan
/5 Antonío de Haedo y Dn. Domingo Bauzá
/6 [sello] su Derecho por los daños y perjuicíos de que
/7 solicitan ser reintegrados [¿parra?] deducírle
/8 [ilegible] el Juicío de Residencia del referido Go-
/9 -vernador y del Virrey. Fecho en Madrid á
/10 quatro de Diciembre de mil setecientos ochen-
/11 -ta y quatro= Yo el Rey= Por mandando
/12 del Rey Nuestro Señor= Don Miguel de San
/13 Martin [¿Prieto?]= Haí tres rubricas=
/14 Para el Virrey de Buenos-Ayres, sobre queja
/15 de Don Juan Antonío de Haedo y Don Domin-
/16 -go Bauzá, como Alcaldes ordinarios en
/17 Montevideo, contra aquel Governador en
/18 punto á Jurisisdiccíon=–
/19 [...]cuerda con la R.l Cedula original de su contexto que
/20 [¿se?] halla en el Exped.te de la materia que por ahora queda
/21 el archibo de esta [¿Colonia?] Mayor de la Govern.on y [ilegible]
/22 [¿que?] está á mi cargo; y de mandato[?] del exmo. Sôr. Virrey
/23 Gobern.or y Cap.n Gen.l de estas Provinc.s del Río de la Plata,
/24 [...] autorizó y firmo en Buenos-Ayres á trece de Mayo
/25 de mil setecientos ochenta y seís años#
/26 Fran.co Ant.o de Basavilbaso