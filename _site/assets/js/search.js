// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/

// Create the lunr index for the search
var index = elasticlunr(function () {
  this.addField('title')
  this.addField('author')
  this.addField('layout')
  this.addField('content')
  this.setRef('id')
});

// Add to this index the proper metadata from the Jekyll content

index.addDoc({
  title: "404: Page not found",
  author: null,
  layout: "page",
  content: "Sorry, we've misplaced that URL or it's pointing to something that doesn't exist.\nHead back home to try finding it again.\n",
  id: 0
});
index.addDoc({
  title: null,
  author: null,
  layout: "default",
  content: "\n  Universidad de la República\n    Facultad de Humanidades y Ciencias de la Educación\n    Instituto de Ciencias Históricas\n    Departamento de historiología\n    Técnicas de la investigación histórica\n    \n    Presentación de las copias paleográficas de la primera prueba parcial\n    \n    \n    Federico Sardi\n      a.federicosardi@gmail.com\n      C.I.: 4.642.192-1\n      \n      Equipo docente:\n      Prof. Agr. Juan Andrés Bresciano\n      Asist. Santiago Delgado\n      Ay. Matías Borba  \n\n",
  id: 1
});
index.addDoc({
  title: "Search",
  author: null,
  layout: "page",
  content: "\nThis is a simple search system. It will match most non-grammatical words.\n  Not all results will be highlighted on the page.\n\n\n\n\n\n\n\n",
  id: 2
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/\r\n\r\n// Create the lunr index for the search\r\nvar index = elasticlunr(function () {\r\n  this.addField('title')\r\n  this.addField('author')\r\n  this.addField('layout')\r\n  this.addField('content')\r\n  this.setRef('id')\r\n});\r\n\r\n// Add to this index the proper metadata from the Jekyll content\r\n{% assign count = 0 %}{% for text in site.pages %}\r\nindex.addDoc({\r\n  title: {{text.title | jsonify}},\r\n  author: {{text.author | jsonify}},\r\n  layout: {{text.layout | jsonify}},\r\n  content: {{text.content | jsonify | strip_html}},\r\n  id: {{count}}\r\n});{% assign count = count | plus: 1 %}{% endfor %}\r\n\r\n// Builds reference data (maybe not necessary for us, to check)\r\nvar store = [{% for text in site.pages %}{\r\n  \"title\": {{text.title | jsonify}},\r\n  \"author\": {{text.author | jsonify}},\r\n  \"layout\": {{ text.layout | jsonify }},\r\n  \"link\": {{text.url | jsonify}},\r\n}\r\n{% unless forloop.last %},{% endunless %}{% endfor %}]\r\n\r\n// Query\r\nvar qd = {}; // Gets values from the URL\r\nlocation.search.substr(1).split(\"&\").forEach(function(item) {\r\n    var s = item.split(\"=\"),\r\n        k = s[0],\r\n        v = s[1] && decodeURIComponent(s[1]);\r\n    (k in qd) ? qd[k].push(v) : qd[k] = [v]\r\n});\r\n\r\nfunction doSearch() {\r\n  var resultdiv = document.querySelector('#results');\r\n  var query = document.querySelector('input#search').value;\r\n\r\n  // The search is then launched on the index built with Lunr\r\n  var result = index.search(query);\r\n  resultdiv.innerHTML = \"\";\r\n  if (result.length == 0) {    \r\n    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');\r\n  } else if (result.length == 1) {\r\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');\r\n  } else {\r\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');\r\n  }\r\n  // Loop through, match, and add results\r\n  for (var item in result) {\r\n    var ref = result[item].ref;\r\n    res = document.createElement('div')\r\n    res.classList.add(\"result\")\r\n    link = document.createElement('a')\r\n    link.setAttribute('href', '{{site.baseurl}}'+store[ref].link+'?q='+query)\r\n    link.innerHTML = store[ref].title || 'Untitled page'\r\n    p = document.createElement('p')\r\n    p.appendChild(link)\r\n    res.appendChild(p)\r\n    resultdiv.appendChild(res)\r\n  }\r\n}\r\n\r\nvar callback = function(){\r\n  searchInput = document.querySelector('input#search') \r\n  if (qd.q) {\r\n    searchInput.value = qd.q[0];\r\n    doSearch();\r\n  }\r\n  searchInput.addEventListener('keyup', doSearch);\r\n};\r\n\r\nif (\r\n    document.readyState === \"complete\" ||\r\n    (document.readyState !== \"loading\" && !document.documentElement.doScroll)\r\n) {\r\n  callback();\r\n} else {\r\n  document.addEventListener(\"DOMContentLoaded\", callback);\r\n}\r\n",
  id: 3
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "/*\n  Common Variables\n\n  Feel free to change!\n*/\n\n/* Fonts */\n$main-font: \"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;\n$heading-font: sans-serif;\n$regular-font-size: 1.25em; /* 20px / 16px = 1.25em; support text resizing in all browsers */\n\n\n/*\n  Color\n\n  Make sure to leave color-scheme in `_config.yml` file empty for granular control\n*/\n\n$text-color: #454545;\n$heading-color: #404040;\n$link-color: #841212;\n\n@import \"ed\";\n@import \"syntax\";\n@import \"CETEIcean.css\";\n",
  id: 4
});
index.addDoc({
  title: "Copia paleográfica",
  author: null,
  layout: "tei",
  content: "\n  Transcripción paleográfica de la Real Cédula de 4 de diciembre de 1784:\n\n",
  id: 5
});

// Builds reference data (maybe not necessary for us, to check)
var store = [{
  "title": "404: Page not found",
  "author": null,
  "layout": "page",
  "link": "/404.html",
}
,{
  "title": null,
  "author": null,
  "layout": "default",
  "link": "/",
}
,{
  "title": "Search",
  "author": null,
  "layout": "page",
  "link": "/_pages/search/",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/js/search.js",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/css/style.css",
}
,{
  "title": "Copia paleográfica",
  "author": null,
  "layout": "tei",
  "link": "/_pages/tei_example/",
}
]

// Query
var qd = {}; // Gets values from the URL
location.search.substr(1).split("&").forEach(function(item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]);
    (k in qd) ? qd[k].push(v) : qd[k] = [v]
});

function doSearch() {
  var resultdiv = document.querySelector('#results');
  var query = document.querySelector('input#search').value;

  // The search is then launched on the index built with Lunr
  var result = index.search(query);
  resultdiv.innerHTML = "";
  if (result.length == 0) {    
    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');
  } else if (result.length == 1) {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');
  } else {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');
  }
  // Loop through, match, and add results
  for (var item in result) {
    var ref = result[item].ref;
    res = document.createElement('div')
    res.classList.add("result")
    link = document.createElement('a')
    link.setAttribute('href', '/paleografia'+store[ref].link+'?q='+query)
    link.innerHTML = store[ref].title || 'Untitled page'
    p = document.createElement('p')
    p.appendChild(link)
    res.appendChild(p)
    resultdiv.appendChild(res)
  }
}

var callback = function(){
  searchInput = document.querySelector('input#search') 
  if (qd.q) {
    searchInput.value = qd.q[0];
    doSearch();
  }
  searchInput.addEventListener('keyup', doSearch);
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  callback();
} else {
  document.addEventListener("DOMContentLoaded", callback);
}
